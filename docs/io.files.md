::: pynapple.io.file
	handler: python
	selection:
		docstring_style: numpy
		members:
			- NPZFile
			- NWBFile
	rendering:
		show_root_heading: false
		show_source: true
		show_category_heading: false
		members_order: source